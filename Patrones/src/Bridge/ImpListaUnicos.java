package Bridge;
import java.util.ArrayList;
public class ImpListaUnicos implements I_ImplLista {
	
	private ArrayList<String> listaItems = new ArrayList<String>();
	
	public void addItem(String item){
		if(!listaItems.contains(item)){
			listaItems.add(item);
		}
	}
	public void remItem(String item){
		if(listaItems.contains(item)){
			listaItems.remove(listaItems.indexOf(item));
		}
	}
	public int getCantidadDeItems(){
		return listaItems.size();
	}
	public String getItem(int index){
		if(index < listaItems.size()){
			return (String) listaItems.get(index);
		}
		return null;
	}
	public boolean soportaRepetidos(){
		return false;
	}
	

}
