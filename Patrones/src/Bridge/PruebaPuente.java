package Bridge;
import java.util.Scanner;
public class PruebaPuente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entrada = new Scanner(System.in);
		int opcion;
		
		I_ImplLista implementacion;
		
		System.out.println("\n Digite el tipo de lista que desea: \n");
		System.out.println("1 = Items Repetidos, 2 = Items �nicos\n");
		
		opcion = entrada.nextInt();
		
		switch(opcion){
		case 1:
			System.out.println("Creando lista. Permite repetidos\n");
			implementacion = new ImpListaRepetidos();
			break;
		case 2:
			System.out.println("Creando lista. Items �nicos\n");
			implementacion = new ImpListaUnicos();
			break;
			default:
				System.out.println("Error, debe elegir entre 1 � 2.");
				System.out.println("Saliendo del programa...");
				return;
		}
		
		System.out.println("Creando presentacion BASE");
		ListaBase listaBase = new ListaBase();
		listaBase.setImplementacion(implementacion);
		System.out.println("Lista Base creada!\n");
		
		System.out.println("Por favor digite 5 elementos de la lista: ");
		for(int i = 0; i < 5; i++){
			System.out.println("Item "+(i+1)+": ");
			listaBase.agregarItem(entrada.next());
		}
		System.out.println();
		
		System.out.println("Creando una lista Enumerada...");
		ListaEnumerada listaEnumerada = new ListaEnumerada();
		listaEnumerada.setImplementacion(implementacion);
		System.out.println();
		
		System.out.println("Creando una lista con Vinetas...");
		ListaVineta listaVineta = new ListaVineta();
		listaVineta.setImplementacion(implementacion);
		System.out.println("Digite un simbolo para la vineta: ");
		listaVineta.setTipoItem((char) entrada.next().charAt(0));
		System.out.println();
		
		System.out.println("Imprimiendo las diferentes listas...");
		//
		System.out.println("Lista Base: ");
		for(int i=0; i<listaBase.cuentaItems(); i++){
			System.out.println("\t"+listaBase.obtenerItem(i));
		}
		System.out.println("Lista Enumerada:");
		for(int i=0; i<listaBase.cuentaItems(); i++){
			System.out.println("\t"+listaEnumerada.obtenerItem(i));
		}
		System.out.println("Lista Vinetas:");
		for(int i=0; i<listaVineta.cuentaItems(); i++){
			System.out.println("\t"+listaVineta.ObtenerItem(i));
		}
	}

}
